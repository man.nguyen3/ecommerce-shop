package com.ecommerce.controllers;


import com.ecommerce.dto.*;
import com.ecommerce.dto.User.SignInDto;
import com.ecommerce.dto.User.SignInResponseDto;
import com.ecommerce.dto.User.SignupDto;
import com.ecommerce.exceptions.AuthenticationFailException;
import com.ecommerce.exceptions.CustomException;
import com.ecommerce.model.user.User;
import com.ecommerce.repository.UserRepository;
import com.ecommerce.service.AuthenticationService;
import com.ecommerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    UserService userService;

    @GetMapping("/all")
    public List<User> findAllUser(@RequestParam("token") String token) throws AuthenticationFailException {
        authenticationService.authenticate(token);
        return userRepository.findAll();
    }

    @PostMapping("/signup")
    public ResponseDto Signup(@RequestBody SignupDto signupDto) throws CustomException {
        return userService.signUp(signupDto);
    }

    //TODO token should be updated
    @PostMapping("/signIn")
    public SignInResponseDto Signup(@RequestBody SignInDto signInDto) throws CustomException {
        return userService.signIn(signInDto);
    }
}