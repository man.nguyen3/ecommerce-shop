package com.ecommerce.controllers;

import com.ecommerce.common.ApiResponse;
import com.ecommerce.dto.Product.ProductCreateRatingDto;
import com.ecommerce.exceptions.AuthenticationFailException;
import com.ecommerce.exceptions.ProductNotExistException;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.rating.Rating;
import com.ecommerce.model.user.User;
import com.ecommerce.repository.RatingRepository;
import com.ecommerce.service.AuthenticationService;
import com.ecommerce.service.ProductService;
import com.ecommerce.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rating")
public class RatingController {

    @Autowired
    private ProductService productService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse> create(@RequestBody ProductCreateRatingDto productRatingDto,
            @RequestParam("token") String token) throws AuthenticationFailException, ProductNotExistException {
        // authenticate the user
        authenticationService.authenticate(token);
        // get the user and product
        User user = authenticationService.getUser(token);
        Product product = productService.getProductById(productRatingDto.getProductId());
        // create the rating
        ratingService.create(productRatingDto, product, user);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, ""), HttpStatus.OK);
    }

    @GetMapping("/{productId}/ratings")
    public ResponseEntity<List<Map<String, Object>>> getRatingByProduct(@PathVariable Integer productId)
            throws ProductNotExistException {
        List<Rating> ratingList = ratingService.getRatingByProduct(productId);
        List<Map<String, Object>> result = new ArrayList<>();
        for (Rating rating : ratingList) {
            Map<String, Object> ratingMap = new HashMap<>();
            ratingMap.put("id", rating.getId());
            ratingMap.put("star", rating.getStar());
            ratingMap.put("feedBack", rating.getFeedBack());
            ratingMap.put("createdDate", rating.getCreatedDate());
            result.add(ratingMap);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/product/{productId}/user/{userId}/ratings")
    public ResponseEntity<List<Rating>> getRatingByProductAndUser(@PathVariable Integer productId,
            @RequestParam Integer userId) throws ProductNotExistException {
        List<Rating> ratings = ratingService.getRatingByProductAndUser(productId, userId);
        return new ResponseEntity<>(ratings, HttpStatus.OK);
    }
}
