package com.ecommerce.model.rating;

import com.ecommerce.model.product.Product;
import com.ecommerce.model.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "ratings")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    private Integer star;
    private String feedBack;
    @Column(name = "created_date")
    private Date createdDate;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    @JsonIgnore
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public Rating( Product product,User user,Integer star,String feedBack){
        this.createdDate = new Date();
        this.user = user;
        this.product = product;
        this.feedBack = feedBack;
        this.star = star;
    }
    public Rating(Product product, User user) {
        this.product = product;
        this.user = user;
    }    
}
