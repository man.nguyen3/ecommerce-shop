package com.ecommerce.repository;

import com.ecommerce.model.rating.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating,Integer> {
    List<Rating> findByProductIdAndUserId(Integer productId,Integer userId);
    List<Rating> findByProductId(Integer userId);
}
