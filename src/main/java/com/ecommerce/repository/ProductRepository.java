package com.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.model.product.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}
