package com.ecommerce.dto.Product;

import com.ecommerce.model.rating.Rating;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductCreateRatingDto {
    private Integer id;
    private @NotNull Integer userId;
    private @NotNull Integer productId;
    private Integer star;
    private String feedBack;
    public ProductCreateRatingDto(Rating rating) {
    }
}

