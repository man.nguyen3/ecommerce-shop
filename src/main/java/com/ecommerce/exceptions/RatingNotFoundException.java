package com.ecommerce.exceptions;

class RatingNotFoundException extends RuntimeException {
    RatingNotFoundException(String message) {
        super(message);
    }
}
