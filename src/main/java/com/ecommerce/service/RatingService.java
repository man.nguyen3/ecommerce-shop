package com.ecommerce.service;

import com.ecommerce.dto.Product.ProductCreateRatingDto;
import com.ecommerce.exceptions.ProductNotExistException;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.rating.Rating;
import com.ecommerce.model.user.User;
import com.ecommerce.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RatingService {
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private ProductService productService;

    public void create(ProductCreateRatingDto ratingDto, Product product, User user) {
        Optional<Rating> optionalRating = ratingRepository.findByProductIdAndUserId(product.getId(), user.getId()).stream().findFirst();
        Rating rating;
        if (optionalRating.isPresent()) {
            rating = optionalRating.get();
            rating.setStar(ratingDto.getStar());
            rating.setFeedBack(ratingDto.getFeedBack());
        } else {
            rating = new Rating(product, user, ratingDto.getStar(), ratingDto.getFeedBack());
        }
        ratingRepository.save(rating);
    }

    public List<Rating> getRatingByProduct(Integer productId) throws ProductNotExistException {
        productService.getProductById(productId);
        return ratingRepository.findByProductId(productId);
    }

    public List<Rating> getRatingByProductAndUser(Integer productId, Integer userId) throws ProductNotExistException {
        productService.getProductById(productId);
        return ratingRepository.findByProductIdAndUserId(productId, userId);
    }
}
